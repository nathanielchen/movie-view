// ------------------------------------
// Constants
// ------------------------------------
export const ADD_MOVIE = 'ADD_MOVIE'
export const REMOVE_MOVIE = 'REMOVE_MOVIE'

// ------------------------------------
// Actions
// ------------------------------------

export function addMovie (movie) {
  return (dispatch, getState) => {
    return new Promise(resolve => {
      setTimeout(() => {
        dispatch({
          type: ADD_MOVIE,
          payload: getState().myMovies
        })
        resolve()
      }, 200)
    })
  }
}

export function removeMovie (id) {
  return (dispatch, getState) => {
    return new Promise(resolve => {
      setTimeout(() => {
        dispatch({
          type: REMOVE_MOVIE,
          payload: getState().myMovies
        })
        resolve()
      }, 200)
    })
  }
}

export const actions = {
  addMovie,
  removeMovie
}

// ------------------------------------
// Action Handlers
// ------------------------------------
const ACTION_HANDLERS = {
  [ADD_MOVIE]: (state, action) => {},
  [REMOVE_MOVIE]: (state, action) => {}
}
// state + action.payload,
// state * 2

// ------------------------------------
// Reducer
// ------------------------------------
const initialState = 0
export default function counterReducer (state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]

  return handler ? handler(state, action) : state
}
