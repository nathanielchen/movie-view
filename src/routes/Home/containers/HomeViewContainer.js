import { connect } from 'react-redux'
import { addMovie, removeMovie } from '../modules/homeViewModule'

/*  This is a container component. Notice it does not contain any JSX,
    nor does it import React. This component is **only** responsible for
    wiring in the actions and state necessary to render a presentational
    component - in this case, the counter:   */

import HomeView from '../components/HomeView'

const mapDispatchToProps = {
  addMovie: movie => addMovie(movie),
  removeMovie: id => removeMovie(id)
}

const mapStateToProps = state => ({
  myMovies: state.myMovies
})

export default connect(mapStateToProps, mapDispatchToProps)(HomeView)
