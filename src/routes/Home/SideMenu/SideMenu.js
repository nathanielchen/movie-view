import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Logo from '../assets/telly.png'
import './SideMenu.scss'

type Props = {
  genres: Array,
  myMovies: Array,
  className: String
}

class SideMenu extends Component<Props> {
  constructor (props: Props) {
    super(props)

    this.state = {
      errored: false
    }
  }

  handleError = event => {
    this.setState({ errored: true })
  }

  render () {
    if (this.state.errored) {
      return null
    } else {
      return (
        <div className={`side_menu animate ${this.props.className}`}>
          <div className='logo_container'>
            <img className='logo' src={Logo} />
          </div>

          <div>
            <div className='side_menu__title'>BROWSE TELLY</div>
            <ul>
              <li title='DISCOVER' onClick={event => this.props.onBrowse(event.target.title)}>
                DISCOVER
              </li>
              <li title='MYMOVIES' onClick={event => this.props.onBrowse(event.target.title)}>
                MY MOVIES <span className='badge'>{this.props.myMovies.length}</span>
              </li>
            </ul>
          </div>
          <div className='categories'>
            <div className='side_menu__title'>CATEGORIES</div>
            <ul>
              <li title='' onClick={event => this.props.onCategorySelected(event.target.title)}>
                All
              </li>
              {this.props.genres.map((genre, index) => (
                <li key={index} title={genre} onClick={event => this.props.onCategorySelected(event.target.title)}>
                  {genre}
                </li>
              ))}
            </ul>
          </div>
        </div>
      )
    }
  }
}

export default SideMenu

SideMenu.propTypes = {
  genres: PropTypes.array.isRequired,
  onBrowse: PropTypes.func.isRequired,
  onCategorySelected: PropTypes.func.isRequired,
  myMovies: PropTypes.array,
  className: PropTypes.string
}
