import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Movie from './Movie'
import './Discover.scss'

type Props = {
  movies: Array,
  genres: Array,
  isSearched: Function,
  onAddWatchLater: Function,
  onRemoveWatchLater: Function,
  isMyMovieById: Function
}

class Discover extends Component<Props> {
  render () {
    return (
      <div>
        <h2 className='page_name'>Discover</h2>
        {this.props.genres.map(genre => (
          <div>
            <h2 className='movie_genres'>{genre}</h2>
            <ul className='discover_movies'>
              {this.props.movies
                .filter(this.props.isSearched('', genre))
                .map(movie => (
                  <Movie
                    key={movie.id}
                    onAddWatchLater={this.props.onAddWatchLater}
                    onRemoveWatchLater={this.props.onRemoveWatchLater}
                    movie={movie}
                    isMyMovie={this.props.isMyMovieById(movie.id)}
                  />
                ))}
            </ul>
          </div>
        ))}
      </div>
    )
  }
}
Discover.propTypes = {
  movies: PropTypes.arrayOf(PropTypes.object),
  genres: PropTypes.array,
  isSearched: PropTypes.func,
  onAddWatchLater: PropTypes.func,
  onRemoveWatchLater: PropTypes.func,
  isMyMovieById: PropTypes.func
}

export default Discover
