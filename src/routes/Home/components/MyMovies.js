import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Movie from './Movie'
import './MyMovies.scss'

type Props = {
  movies: Array,
  onRemoveWatchLater: Function
}

class MyMovies extends Component<Props> {
  render () {
    let message = null
    if (this.props.movies.length <= 0) {
      message = (
        <h2 className='message'>
          By adding movie to your Watch Later playlist, you can easily access them whenever you want.
        </h2>
      )
    }

    return (
      <div>
        <h2 className='page_name'>My Movie</h2>
        {message}
        <ul className='movies'>
          {this.props.movies.map((movie, index) => (
            <Movie
              key={movie.id}
              onRemoveWatchLater={this.props.onRemoveWatchLater}
              index={index}
              isMyMovie
              movie={movie}
            />
          ))}
        </ul>
      </div>
    )
  }
}

MyMovies.propTypes = {
  movies: PropTypes.arrayOf(PropTypes.object),
  onRemoveWatchLater: PropTypes.func
}

export default MyMovies
