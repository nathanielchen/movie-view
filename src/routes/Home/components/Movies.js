import React from 'react'
import PropTypes from 'prop-types'
import Movie from './Movie'
import './Movies.scss'

const Movies = props => (
  <ul className='movies'>
    {props.movies.map(movie => (
      <Movie
        key={movie.id}
        onAddWatchLater={props.onAddWatchLater}
        onRemoveWatchLater={props.onRemoveWatchLater}
        movie={movie}
        isMyMovie={props.isMyMovieById(movie.id)}
      />
    ))}
  </ul>
)

Movies.propTypes = {
  movies: PropTypes.arrayOf(PropTypes.object),
  onAddWatchLater: PropTypes.func,
  onRemoveWatchLater: PropTypes.func,
  isMyMovieById: PropTypes.func
}

export default Movies
