import React, { Component } from 'react'
import './Movie.scss'
import PropTypes from 'prop-types'

type Props = {
  movie: string,
  onAddWatchLater: Function,
  onRemoveWatchLater: Function,
  isMyMovie: Boolean
}

class Movie extends Component<Props> {
  constructor (props: Props) {
    super(props)

    this.state = {
      errored: false,
      selected: false
    }
  }

  handleError = event => {
    this.setState({ errored: true })
  }

  selectedHandler = event => {
    this.setState({ selected: true })
  }

  deselectedHandler = event => {
    this.setState({ selected: false })
  }

  render () {
    let movieInfo = null
    let controlButton = null

    if (this.state.selected) {
      movieInfo = (
        <h3 className='movie_info'>{`${this.props.movie.genres[0]} - ${this.props.movie.runtime}min - ${
          this.props.movie.year
        }`}</h3>
      )
      if (this.props.isMyMovie) {
        controlButton = (
          <button
            className='movie_watch_later__button'
            onClick={event => this.props.onRemoveWatchLater(this.props.movie.id)}
          >
            Remove
          </button>
        )
      } else {
        controlButton = (
          <button className='movie_watch_later__button' onClick={event => this.props.onAddWatchLater(this.props.movie)}>
            Watch Later
          </button>
        )
      }
    }

    if (this.state.errored) {
      return null
    } else {
      return (
        <li onMouseOver={this.selectedHandler} onMouseLeave={this.deselectedHandler}>
          <div className={`movie${this.props.isMyMovie ? ' movie_selected' : ''}`}>
            <div className='movie_item'>
              <img onError={this.handleError} src={this.props.movie.posterUrl} className='movie_poster' />
              <h2 className='movie_title'>{this.props.movie.title}</h2>
              {movieInfo}
              {controlButton}
            </div>
          </div>
        </li>
      )
    }
  }
}

export default Movie

Movie.propTypes = {
  movie: PropTypes.object.isRequired,
  onAddWatchLater: PropTypes.func,
  onRemoveWatchLater: PropTypes.func,
  isMyMovie: PropTypes.bool
}

Movie.defaultProps = {
  isMyMovie: false
}
