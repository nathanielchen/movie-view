import React from 'react'

import Movies from './Movies'
import Search from './Search'
import SideMenu from '../SideMenu/SideMenu'
import MyMovies from './MyMovies'
import Discover from './Discover'
import PropTypes from 'prop-types'

import './HomeView.scss'

import userAvatar from '../assets/ed-sticker.png'
const url = '../assets/movies.json'

type Props = {
  myMovies: Object,
  addMovie: Function,
  removeMovie: Function
}

class HomeView extends React.Component<Props> {
  constructor (props: Props) {
    super(props)
    console.log(this.props.myMovies)

    this.state = {
      movies: [],
      myMovies: [],
      genres: [],
      query: '',
      category: '',
      showSideMenu: false
    }

    fetch(url)
      .then(response => response.json())
      .then(data => {
        this.setState({
          movies: data.movies,
          genres: data.genres
        })
      })

    this.onInput = this.onInput.bind(this)
    this.onCategorySelected = this.onCategorySelected.bind(this)
    this.onBrowse = this.onBrowse.bind(this)
    this.onAddWatchLater = this.onAddWatchLater.bind(this)
    this.onRemoveWatchLater = this.onRemoveWatchLater.bind(this)
    this.onShowSideMenu = this.onShowSideMenu.bind(this)
    this.onHideSideMenu = this.onHideSideMenu.bind(this)
    this.isMyMovieById = this.isMyMovieById.bind(this)
    this.inMyMovie = this.inMyMovie.bind(this)
  }

  onInput (query) {
    this.setState({
      query,
      page: ''
    })

    this.searchMovie()
  }

  onShowSideMenu () {
    this.setState({
      showSideMenu: true
    })
  }

  onHideSideMenu () {
    this.setState({
      showSideMenu: false
    })
  }

  searchMovie () {
    fetch(url)
      .then(response => response.json())
      .then(data => {
        this.setState({
          movies: data.movies
        })
      })
  }

  onBrowse (page) {
    this.setState({
      page: page
    })

    this.searchMovie()
  }

  onCategorySelected (category) {
    if (this.category !== category) {
      this.setState({
        category,
        query: '',
        page: ''
      })

      this.searchMovie()
    }
  }

  onAddWatchLater (movie) {
    if (movie) {
      let myMovies = [...this.state.myMovies]

      myMovies.push(movie)

      this.setState({
        myMovies: myMovies
      })
    }
  }

  onRemoveWatchLater (id) {
    this.props.removeMovie(id)

    let myMovies = [...this.state.myMovies]

    let movieIndex = myMovies.findIndex(movie => {
      return movie.id === id
    })

    console.log(movieIndex)

    myMovies.splice(movieIndex, 1)

    this.setState({
      myMovies: myMovies
    })
  }

  isMyMovieById = id => {
    let myMovies = [...this.state.myMovies]

    let foundMovie = myMovies.find(movie => {
      return movie.id === id
    })

    if (foundMovie !== undefined) {
      return true
    }
    return false
  }

  inMyMovie = movie => {
    return this.state.myMovies.includes(movie)
  }

  render () {
    const { movies, query, genres, category, myMovies, page } = this.state
    const isSearched = (query, category) => movie => {
      return (
        (!query || movie.title.toLowerCase().includes(query.toLowerCase())) &&
        (!category || movie.genres.includes(category))
      )
    }

    let mainPage = null

    switch (page) {
      case 'MYMOVIES':
        mainPage = <MyMovies movies={myMovies} onRemoveWatchLater={this.onRemoveWatchLater} />
        break
      case 'DISCOVER':
        mainPage = (
          <Discover
            movies={movies}
            genres={genres}
            isSearched={isSearched}
            onRemoveWatchLater={this.onRemoveWatchLater}
            onAddWatchLater={this.onAddWatchLater}
            isMyMovieById={this.isMyMovieById}
          />
        )
        break
      default:
        mainPage = (
          <div>
            <div className='header'>
              <div className='control-wrapper inline'>
                <button className='menu-button' onClick={this.onShowSideMenu}>
                  ☰
                </button>
              </div>

              <div className='header-container'>
                <div className='inline'>
                  <Search className='inline' query={query} onInput={this.onInput} placeholder='Search for Movie' />
                  <img className='avatar inline' src={userAvatar} />
                  <div className='account_wrapper inline'>
                    <h4 className='user_name'>Romain Guillaume</h4>
                  </div>
                </div>
              </div>
            </div>

            <h2 className='movie_genres'>{category}</h2>
            <Movies
              onAddWatchLater={this.onAddWatchLater}
              onRemoveWatchLater={this.onRemoveWatchLater}
              movies={movies.filter(isSearched(query, category))}
              isMyMovieById={this.isMyMovieById}
            />
          </div>
        )
    }

    return movies.length > 0 ? (
      <div className='app'>
        <div className='main_view'>{mainPage}</div>
        <div className={this.state.showSideMenu ? 'black back-drop' : 'none_mobile'} onClick={this.onHideSideMenu} />

        <SideMenu
          className={this.state.showSideMenu ? 'black' : 'none_mobile'}
          genres={genres}
          myMovies={myMovies}
          onCategorySelected={this.onCategorySelected}
          onBrowse={this.onBrowse}
        />
      </div>
    ) : null
  }
}

HomeView.propTypes = {
  myMovies: PropTypes.number.isRequired,
  addMovie: PropTypes.func.isRequired,
  removeMovie: PropTypes.func.isRequired
}

export default HomeView
