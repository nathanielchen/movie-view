// We only need to import the modules necessary for initial render
import CoreLayout from '../layouts/PageLayout/PageLayout'
import Home from './Home'
import UserAccount from './UserAccount'
// import UserAccountRoute from './UserAccount'

/*  Note: Instead of using JSX, we recommend using react-router
    PlainRoute objects to build route definitions.   */

export const createRoutes = store => [
  {
    path: '/',
    component: CoreLayout,
    indexRoute: Home(store)
  },
  {
    path: '/user',
    component: CoreLayout,
    indexRoute: UserAccount(store)
  }
]
// Add child Routes with store
//, childRoutes: [UserAccountRoute(store)]

export default createRoutes
