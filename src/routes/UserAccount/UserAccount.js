import React, { Component } from 'react'

type Props = {
  myUser: Object
}

class UserAccount extends Component<Props> {
  render () {
    return (
      <div>
        {this.props.myUser.firstName}
        <br />
        {this.props.myUser.lastName}
      </div>
    )
  }
}

export default UserAccount
